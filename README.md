# s2it-desafio

**1** - A
**2** - D
**3** - C
**4** - A
**5** - A
**6** - C
**7** - E

**8** - Dados dois numeros inteiros A e B, crie um terceiro inteiro C seguindo as seguintes
regras:
- O primeiro n�mero de C � o primeiro n�mero de A;
- O segundo n�mero de C � o primeiro n�mero de B;
- O terceiro n�mero de C � o segundo n�mero de A;
- O quarto n�mero de C � o segundo n�mero de B;
Assim sucessivamente�
- Caso os n�meros de A ou B sejam de tamanhos diferentes, completar C com o restante dos n�meros do inteiro maior. Ex: A = 10256, B = 512, C deve ser 15012256.
- Caso C seja maior que 1.000.000, retornar -1
Desenvolva um algoritmo que atenda a todos os requisitos acima.


**9** - Considerando a estrutura de uma �rvore bin�ria:

```Java
public class BinaryTree {

       int valor;
       
       BinaryTree left;
       
       BinaryTree right;
       
}
```

Desenvolva um m�todo que dado um n� da �rvore calcule a soma de todos os n�s subsequentes.