package desafio.question9;

import java.util.Objects;

public class CalcSum {

    private BinaryTree binaryTree;

    public CalcSum(BinaryTree binaryTree) {
        Objects.requireNonNull(binaryTree, "A �rvore bin�ria n�o pode ser nula");
        this.binaryTree = binaryTree;
    }

    public int sum() {
        return sum(binaryTree);
    }

    private int sum(BinaryTree binaryTree) {

        if (binaryTree == null) {
            return 0;
        }
        return binaryTree.getValor() + sum(binaryTree.getLeft()) + sum(binaryTree.getRight());
    }
}